require "Window"
require "Apollo"
require "ICCommLib"
require "ICComm"

local GroupFinder = { }

GroupFinder.ADDON_VERSION = 0.4

GroupFinder.ListingType = {
    LFM = 1,
    LFG = 2,
    CHAT = 3
}

GroupFinder.ListingTypeData = {
    [GroupFinder.ListingType.LFM] = {
        id = GroupFinder.ListingType.LFM,
        name = "Looking for More",
        showInCreateListing = true
    },
    [GroupFinder.ListingType.LFG] = {
        id = GroupFinder.ListingType.LFG,
        name = "Looking for Group",
        showInCreateListing = true
    },
    [GroupFinder.ListingType.CHAT] = {
        id = GroupFinder.ListingType.CHAT,
        name = "Chat Listing",
        showInCreateListing = false
    }
}

GroupFinder.Timers = {
    ["Periodic_Remove"] = 10,
    ["Chat_Remove"] = 900,
    ["Listing_Remove"] = 90,
    ["Periodic_Update"] = 30
}

local NavigationButton = {
    GROUP_FINDER = 1,
    FIND_PREMADE = 2,
    CREATE_PREMADE = 3
}

local NavigationButtonData = {
    [NavigationButton.GROUP_FINDER] = {
        name = "GroupFinderButton"
    },
    [NavigationButton.FIND_PREMADE] = {
        name = "FindPremadeButton"
    },
    [NavigationButton.CREATE_PREMADE] = {
        name = "CreatePremadeButton"
    }
}

local MatchMakerAddons = {
    ["MatchMaker"] = {
        wndMain = "wndMain",
        strHeader = "VendorName",
        strAnchor = "BGArt",
        strToggleEvent = "OnToggleMatchMaker",
        strEvent = "OnMatchMakerOn"
    },
    ["EasyMatchMaker"] = {
        wndMain = "wndMain",
        strHeader = "VendorName",
        strAnchor = "BGArt",
        strToggleEvent = "OnToggleMatchMaker",
        strEvent = "OnEasyMatchMakerOn"
    }
}

local strParentAddon = "MatchMaker"
 
function GroupFinder:new(o)
    o = o or { }
    setmetatable(o, self)
    self.__index = self

    o.nNavigationButtonChecked = 1
    o.nCreateListingType = GroupFinder.ListingType.LFM
    o.nFindListingType = GroupFinder.ListingType.LFM
    o.nPremadeListCurrentSelectedRow = 0
    o.bSetup = true
    o.xmlData = { }
    o.tGameModeList = { }
    o.tPremadeList = { }

    return o
end

function GroupFinder:Init()
    local tDependencies = {
        "Gemini:Hook-1.0",
        "MatchMaker"
    }
    Apollo.RegisterAddon(self, false, "", tDependencies)
end

function GroupFinder:OnDependencyError(strDep, strError)
    if strDep == "MatchMaker" then
        local tReplacedBy = Apollo.GetReplacement(strDep)
        if #tReplacedBy ~= 1 or not MatchMakerAddons[tReplacedBy[1]] then
            return false
        end
        strParentAddon = tReplacedBy[1]
        return true
    end
    return false
end

function GroupFinder:OnLoad()

    local nIndex = 1
    for k, v in ipairs(XmlDoc.CreateFromFile("toc.xml"):ToTable()) do
        if v.__XmlNode == "DocData" then
            self.xmlData[nIndex] = XmlDoc.CreateFromFile(Apollo.GetAssetFolder() .. "\\" .. v.Name):ToTable()
            nIndex = nIndex + 1
        end
    end

    self:LoadData()

    self.xmlDoc = XmlDoc.CreateFromFile("GroupFinder.xml")
    self.xmlDoc:RegisterCallback("OnDocumentReady", self)
    Apollo.LoadSprites("GroupFinderSprites.xml")

    Apollo.GetPackage("Gemini:Hook-1.0").tPackage:Embed(self)
end

function GroupFinder:OnDocumentReady()

    self.tMatchMakerAddon = Apollo.GetAddon(strParentAddon)

    self:RawHook(self.tMatchMakerAddon, MatchMakerAddons[strParentAddon].strToggleEvent, "OnToggleMatchMaker")

    self:SetupWindows()
    self:InitNetwork()
    self:InitChat()

    self:DrawGameModes()
    self:DrawModeFlyOut()
    self:DrawCreateLookingFlyOut()
    self:DrawFindLookingFlyOut()
end

function GroupFinder:LoadData()
    for _, v in ipairs(self.xmlData[1]) do
        if v.__XmlNode == "GameModes" then
            for _, gm in ipairs(v) do
                table.insert(self.tGameModeList, { gameType = gm.type, maxPlayers = gm.maxPlayers, name = gm.name, reqLevel = gm.reqLevel, veteranState = gm.veteranState, hideInOverview = gm.hideInOverview })
            end
        end
    end
end

function GroupFinder:OnToggleMatchMaker(luaCaller)
    if self.bSetup then
        self:SetupGroupFinder()
    end
    self:ToggleCurrentFrame(luaCaller)
end

function GroupFinder:SetupGroupFinder()
    self.wndMatchMaker = self.tMatchMakerAddon[MatchMakerAddons[strParentAddon].wndMain]
    self.wndMatchMaker:FindChild(MatchMakerAddons[strParentAddon].strHeader):Show(false)
    self.wndGroupFinderNavigation = Apollo.LoadForm(self.xmlDoc, "Navigation", self.wndMatchMaker:FindChild(MatchMakerAddons[strParentAddon].strAnchor), self)
    self.bSetup = false
end

function GroupFinder:OnModeListGridSelChange(wndControl, wndHandler, iRow, iCol, eMouseButton)
    
    if self.nModeListCurrentSelectedRow == iRow then
        self.nModeListCurrentSelectedRow = 0
        self.wndVeteranMode:Show(false)
        self.wndVeteranMode:SetCheck(false)
    else
        self.nModeListCurrentSelectedRow = iRow
        local tData = wndHandler:GetCellData(self.nModeListCurrentSelectedRow, 1)
        local bShowVeteran = (tData ~= nil and(tData.veteranState == "1" or tData.veteranState == "2"))
        if not bShowVeteran then
            self.wndVeteranMode:Show(false)
            self.wndVeteranMode:SetCheck(false)
        else
            self.wndVeteranMode:Show(true)
        end
    end
    Sound.Play(192)

    wndHandler:SetCurrentRow(self.nModeListCurrentSelectedRow)

    self:SetCreatePremadeButtonState()
end

function GroupFinder:OnNotesEditBoxChanged(wndHandler, wndControl, strText)
    self.wndCharactersLeft:SetText(self.wndGameNotes:GetMaxTextLength() - string.len(strText))
end

function GroupFinder:OnPremadeListGridSelChange(wndControl, wndHandler, iRow, iCol, eMouseButton)
    self.nPremadeListCurrentSelectedRow = (self.nPremadeListCurrentSelectedRow == iRow and 0 or iRow)
    Sound.Play(192)

    self:SetListPremadeButtonStates()
    wndHandler:SetCurrentRow(self.nPremadeListCurrentSelectedRow)
    self.tPremadePreviousData = self:GetGridRowData(wndHandler)
end

function GroupFinder:OnPremadeListGenerateTooltip(wndHandler, wndControl, eType, iRow, iColumn)
    if self.nFindListingType == self.ListingType.LFM then
        self:SetLFMListGenerateTooltip(wndHandler, wndControl, eType, iRow, iColumn)
    elseif self.nFindListingType == self.ListingType.LFG then
        self:SetLFGListGenerateTooltip(wndHandler, wndControl, eType, iRow, iColumn)
    else
        self:SetChatListGenerateTooltip(wndHandler, wndControl, eType, iRow, iColumn)
    end
end

function GroupFinder:SetCreatePremadeButtonState()
    self.wndStartPremadeButton:Enable(self.wndGameModeList:GetCurrentRow() ~= nil and (self:GetCurrentRoles() > 0))
end

function GroupFinder:SetListPremadeButtonStates()
    local bJoinButtonEnabled = false

    local tData = self:GetGridRowData(self.wndCurrentList)
    if self.nFindListingType == self.ListingType.LFG or self.nFindListingType == self.ListingType.CHAT then
        bJoinButtonEnabled = true
    else
        if tData then
            bJoinButtonEnabled = tonumber(string.match(tData.players, "%d+")) > 1
        end
    end

    self.wndFindPremadeWhisperButton:Enable(tData)
    self.wndFindPremadeJoinButton:Enable(tData and bJoinButtonEnabled)
    self.wndFindPremadeRemoveButton:Enable(tData and bJoinButtonEnabled)
end

function GroupFinder:FilterGameMode(wndHandler, wndControl, eMouseButton)
    self.nModeListCurrentSelectedRow = 0
    self.wndGameModeList:SetCurrentRow(self.nModeListCurrentSelectedRow)

    self.wndModeFlyOutToggle:FindChild("Label"):SetText(wndHandler:FindChild("Label"):GetText())
    self.wndModeFlyOut:Show(false)
    self.wndVeteranMode:Show(false)
    self:DrawGameModes()
    self:DrawModeFlyOut()
end

function GroupFinder:ToggleCreateLooking(wndHandler, wndControl, eMouseButton)
    local strButtonLabel = wndHandler:FindChild("Label"):GetText()
    self.wndCreateLookingFlyOutToggle:FindChild("Label"):SetText(strButtonLabel)
    self.wndCreateLookingFlyOut:Show(false)

    local strRolesHeader = self.wndInformationFrame:FindChild("RolesHeader")
    if strButtonLabel == self.ListingTypeData[self.ListingType.LFM].name then
        self.nCreateListingType = self.ListingType.LFM
        strRolesHeader:SetText("Looking for roles:")
    else
        self.nCreateListingType = self.ListingType.LFG
        strRolesHeader:SetText("Set your role:")
    end
end

function GroupFinder:ToggleFindLooking(wndHandler, wndControl, eMouseButton)
    local strButtonLabel = wndHandler:FindChild("Label"):GetText()
    self.wndFindLookingFlyOutToggle:FindChild("Label"):SetText(strButtonLabel)
    self.wndFindLookingFlyOut:Show(false)

    self.nFindListingType = wndHandler:GetData().id
    self:SetListingType(strButtonLabel)

    self:SetListPremadeButtonStates()
    self:DrawPremadeList()

    self.nPremadeListCurrentSelectedRow = 0
    self.wndCurrentList:SetCurrentRow(self.nPremadeListCurrentSelectedRow)
end

function GroupFinder:SetListingType(strButtonLabel)
    self.wndCurrentList:Show(false)

    if self.nFindListingType == self.ListingType.LFM then
        self.wndCurrentList = self.wndLFMList
        self.wndFindPremadeJoinButton:FindChild("Label"):SetText("Join Group")
        self.wndFindPremadeWhisperButton:FindChild("Label"):SetText("Whisper Leader")
        self.wndFindPremadeRemoveButton:Show(false)
    elseif self.nFindListingType == self.ListingType.LFG then
        self.wndCurrentList = self.wndLFGList
        self.wndFindPremadeJoinButton:FindChild("Label"):SetText("Invite to Group")
        self.wndFindPremadeWhisperButton:FindChild("Label"):SetText("Whisper Player")
        self.wndFindPremadeRemoveButton:Show(false)
    elseif self.nFindListingType == self.ListingType.CHAT then
        self.wndCurrentList = self.wndChatList
        self.wndFindPremadeJoinButton:FindChild("Label"):SetText("Join Group")
        self.wndFindPremadeWhisperButton:FindChild("Label"):SetText("Whisper Player")
        self.wndFindPremadeRemoveButton:Show(true)
    end

    self.wndCurrentList:Show(true)
end

function GroupFinder:ToggleCurrentFrame(luaCaller)
    if self.nNavigationButtonChecked == NavigationButton.GROUP_FINDER then
        self.wndCurrentFrame = self.wndMatchMaker
    elseif self.nNavigationButtonChecked == NavigationButton.FIND_PREMADE then
        self.wndCurrentFrame = self.wndFindPremade
        self:SetListPremadeButtonStates()
        self:DrawPremadeList()
    elseif self.nNavigationButtonChecked == NavigationButton.CREATE_PREMADE then
        self.wndCurrentFrame = self.wndCreatePremade
        self:SetCreatePremadeButtonState()
    end

    for _, v in ipairs({ self.wndMatchMaker, self.wndFindPremade, self.wndCreatePremade }) do
        if v == self.wndCurrentFrame then
            if not self.wndCurrentFrame:IsShown() then
                if luaCaller and v == self.wndMatchMaker then
                    luaCaller[MatchMakerAddons[strParentAddon].strEvent](luaCaller)
                else
                    self.wndCurrentFrame:Show(true)
                end
                self.wndCurrentFrame:FindChild(NavigationButtonData[self.nNavigationButtonChecked].name):SetCheck(true)
            else
                self.wndCurrentFrame:Show(false)
                Event_FireGenericEvent("LFGWindowHasBeenClosed")
            end
        else
            v:Show(false)
        end
    end

    local tListingCount = self:DrawPremadeCount()
--    if self.wndCurrentFrame == self.wndFindPremade and #tListingCount > 0 then

--        local tSelectList = {}
--        for i, v in ipairs(tListingCount) do
--            if v > 0 then
--                table.insert(tSelectList, i)
--            end
--        end

--        if #tSelectList > 0 then
--            table.sort(tSelectList, function(a, b) return a[1] > b[1] end)
--            self.nFindListingType = tSelectList[1]
--            self:SetListingType()
--        end
--    end

    self.wndCurrentFrame:ToFront()
end

function GroupFinder:SetGroupFinderChecked()
    self:SetChecked(NavigationButton.GROUP_FINDER)
end
function GroupFinder:SetFindPremadeChecked()
    self:SetChecked(NavigationButton.FIND_PREMADE)
end
function GroupFinder:SetCreatePremadeChecked()
    self:SetChecked(NavigationButton.CREATE_PREMADE)
end

function GroupFinder:SetChecked(button)
    if self.nNavigationButtonChecked ~= button then
        self.nNavigationButtonChecked = button

        local pos = { self.wndCurrentFrame:GetAnchorOffsets() }
        self:ToggleCurrentFrame()
        self.wndCurrentFrame:SetAnchorOffsets(pos[1], pos[2], pos[3], pos[4])
    end
end

-- FIX for FocusOnMouseOver triggering under FlyOut.
function GroupFinder:OnModeFlyOutShown()
    self.wndGameModeList:Enable(false)
    self.wndBlockModeSort:Show(false)
end
function GroupFinder:OnModeFlyOutHidden()
    self.wndGameModeList:Enable(true)
    self.wndBlockModeSort:Show(true)
end

-- FIX for FocusOnMouseOver triggering under FlyOut.
function GroupFinder:OnCreateLookingFlyOutShown()
    self.wndGameNotes:Enable(false)
end
function GroupFinder:OnCreateLookingFlyOutHidden()
    self.wndGameNotes:Enable(true)
end

-- FIX for FocusOnMouseOver triggering under FlyOut.
function GroupFinder:OnFindLookingFlyOutShown()
    self.wndListingsFrame:Enable(false)
end
function GroupFinder:OnFindLookingFlyOutHidden()
    self.wndListingsFrame:Enable(true)
end

function GroupFinder:OnStartPremadeButton(wndHandler, wndControl, eMouseButton)
    wndControl:Show(false)
    self.wndStopPremadeButton:Show(true)
    self.wndUpdatePremadeButton:Show(true)
    self.wndGameFrame:Enable(false)
    self.wndBlockModeFrame:Show(true)
    self.wndCreateLookingFlyOutToggle:Enable(false)

    Apollo.StartTimer("BroadcastPeriodicListingUpdate")

    self:SetListing(self.nCreateListingType)
end

function GroupFinder:OnStopPremadeButton(wndHandler, wndControl, eMouseButton)
    wndControl:Show(false)
    self.wndUpdatePremadeButton:Show(false)
    self.wndStartPremadeButton:Show(true)
    self.wndGameFrame:Enable(true)
    self.wndBlockModeFrame:Show(false)
    self.wndCreateLookingFlyOutToggle:Enable(true)

    Apollo.StopTimer("BroadcastPeriodicListingUpdate")

    self:RemoveListing(self.tOwnListing)
    self:DrawPremadeList()
end

function GroupFinder:OnUpdatePremadeButton(wndHandler, wndControl, eMouseButton)
    self:SetListing(self.nCreateListingType)
end

function GroupFinder:OnJoinPremade(wndHandler, wndControl, eMouseButton)
    local tData = self:GetGridRowData(self.wndCurrentList)
    if tData then
        Apollo.ParseInput((self.wndFindLookingFlyOutToggle:GetData() == self.ListingType.LFG and "/invite " or "/join ") .. tData.creator.name)
    end
end

function GroupFinder:OnRemovePremade(wndHandler, wndControl, eMouseButton)
    local tData = self:GetGridRowData(self.wndCurrentList)
    if tData then
        self:RemoveListing(tData)
        self:SetListPremadeButtonStates()
        self.nPremadeListCurrentSelectedRow = 0
        self.wndCurrentList:SetCurrentRow(self.nPremadeListCurrentSelectedRow)
    end
end

function GroupFinder:OnWhisperPremadeLeader(wndHandler, wndControl, eMouseButton)
    local tData = self:GetGridRowData(self.wndCurrentList)
    if tData then
        local tPlayer = GameLib:GetPlayerUnit()
        local strLevel = tPlayer:GetLevel()
        local strClass = GameLib.GetClassName(tPlayer:GetClassId())

        if tData.listingType == self.ListingType.LFM then
            Apollo.ParseInput("/w " .. tData.creator.name .. " [GroupFinder]: Class: " .. strClass .. ", Level: " .. strLevel .. " wants to join your group.")
        elseif tData.listingType == self.ListingType.LFG then
            Apollo.ParseInput("/w " .. tData.creator.name .. " [GroupFinder]: I want you to join my group." )
        else 
            Apollo.ParseInput("/w " .. tData.creator.name .. " [GroupFinder]: Hello I wish to participate in your chat listing: " .. tData.notes)
        end
    end
end

function GroupFinder:PeriodicListingUpdate()
    if self.tOwnListing then
        self:SetListing(self.nCreateListingType)
    end
end

function GroupFinder:RemoveTimeoutListings()
    for i, v in ipairs(self.tPremadeList) do
        --Print(tostring(os.difftime(os.time(), v.time)))
        if v.listingType ~= self.ListingType.CHAT and os.difftime(os.time(), v.time) > self.Timers["Listing_Remove"] then
            --Print(tostring(os.difftime(os.time(), v.time)))
            self:RemoveListing(v)
        elseif v.listingType == self.ListingType.CHAT and os.difftime(os.time(), v.time) > self.Timers["Chat_Remove"] then
            self:RemoveListing(v)
        end
    end
end

function GroupFinder:SetListing(nListingType)
    local gameMode = self:GetGridRowData(self.wndGameModeList)
    local gameNotes = self.wndGameNotes:GetText()
    local gameRoles = self:GetCurrentRoles()
    local gameVeteran = self.wndVeteranMode:IsChecked()
    local guildAndCircles = self:GetGuildAndCircles()
    local creator = { name = self:GetPlayerName(), guild = guildAndCircles.guild, circles = guildAndCircles.circles }
    local time = os.time()

    local nGroupMembers = GroupLib.GetMemberCount()

    local tMembers = {}
    for i=1,nGroupMembers do
        local tMember = GroupLib.GetGroupMember(i)
        if tMember then
            table.insert(tMembers, { nLevel = tMember.nLevel, strCharacterName = tMember.strCharacterName, strClassName = tMember.strClassName })
        end
    end

    if nGroupMembers == 0 then nGroupMembers = 1 end

    local tListing = { listingType = nListingType, gameType = gameMode.gameType, name = gameMode.name, notes = gameNotes, players = nGroupMembers .. " / " .. gameMode.maxPlayers, roles = gameRoles, veteran = gameVeteran, time = time, creator = creator, members = tMembers }

    self:UpdateListing(tListing, true)
end

function GroupFinder:OnRoleSelection(wndHandler, wndControl, eMouseButton)
    self:SetCreatePremadeButtonState()
end

function GroupFinder:ShowUpdateAddonInfo()
    if not self.bShownOutdated then
        self.bShownOutdated = true
        Sound.Play(149)

        self.wndOutdated:FindChild("Body"):SetText("GroupFinder has been updated!\n\nDownload the latest version on curse.")
        self.wndOutdated:Show(true)
    end
end

function GroupFinder:OnCloseOutdated(wndHandler, wndControl, eMouseButton)
    self.wndOutdated:Show(false)
end

function GroupFinder:OnSave(eLevel)
    if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Account then
        return nil
    end

    local saveData = { }
    saveData.saveVersion = 1

    local tList = { }
    for _, v in ipairs(self.tPremadeList) do
        if v.listingType == self.ListingType.CHAT then
            table.insert(tList, v)
        end
    end
    saveData.tPremadeList = tList

    return saveData;
end

function GroupFinder:OnRestore(eLevel, tData)
    if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Account or tData == nil then
      	return nil
  	end

    if tData.tPremadeList then

  	    local tList = {}
  	    for _, v in ipairs(tData.tPremadeList) do
    	    if os.difftime(os.time(), v.time) < self.Timers["Chat_Remove"] then
      		    table.insert(tList, v)
    	    end
  	    end

  	    self.tPremadeList = tList
    end
end

local GroupFinder = GroupFinder:new()
GroupFinder:Init()