local GroupFinder = Apollo.GetAddon("GroupFinder")
GroupFinder.strPlayerName = nil

function GroupFinder:GetPlayerName()
    if self.strPlayerName then
        return self.strPlayerName
    end

    local playerUnit = GameLib.GetPlayerUnit()
    if playerUnit then
        self.strPlayerName = playerUnit:GetName()
    end

    return self.strPlayerName and self.strPlayerName or ''
end

function GroupFinder:GetGridRowData(tGrid)
    if not tGrid or tGrid:GetCurrentRow() == nil then return nil else return tGrid:GetCellLuaData(tGrid:GetCurrentRow(), 1) end
end

function GroupFinder:CreateButton(strButtonForm, wndAttachTo, strButtonLabel, strButtonSignal, tData)
    local wnd = Apollo.LoadForm(self.xmlDoc, strButtonForm, wndAttachTo, self)
    wnd:FindChild("Label"):SetText(strButtonLabel)
    local btn = wnd:FindChild("Button")
    btn:AddEventHandler("ButtonSignal", strButtonSignal, self)
    if tData then
        btn:SetData(tData)
    end
    --Special handling
    local wndCounter = btn:FindChild("Counter")
    if wndCounter then
        table.insert(self.tListingCounters, { wndCounter })
    end
    return wnd
end

function GroupFinder:GetCurrentRoles()
    local nBit = 0
    if self.wndTankRole:IsChecked() then nBit = nBit + 1 end
    if self.wndHealerRole:IsChecked() then nBit = nBit + 2 end
    if self.wndDPSRole:IsChecked() then nBit = nBit + 4 end
    return nBit
end

function GroupFinder:GetGuildAndCircles()
    local tGuildAndCircles = { guild = nil, circles = {} }
    for _,v in ipairs(GuildLib:GetGuilds()) do
        if v:GetType() == 2 then
            table.insert(tGuildAndCircles.circles, v:GetName())
        elseif v:GetType() == 1 then
            tGuildAndCircles.guild = v:GetName()
        end
    end
    return tGuildAndCircles
end

function GroupFinder:GetMembers(tData)
    if #tData.members == 0 then return nil end
    local strMembers = ""
    for i,v in ipairs(tData.members) do
        strMembers = strMembers .. "[" .. v.nLevel .. "] " .. v.strCharacterName .. " - " .. v.strClassName .. "\n"
    end
    return strMembers
end

function GroupFinder:GetMatchingGuildAndCircles(tData)
    if tData.creator.guild == nil and #tData.creator.circles == 0 then return nil end
    if tData.creator.name == self:GetPlayerName() then return "You are looking at yourself :)" end

    local strMatching = ""
    local hasCircle = false
    local tGuildAndCircles = self:GetGuildAndCircles()
    if tGuildAndCircles then
        if tGuildAndCircles.guild == tData.creator.guild then strMatching = strMatching .. "Guild: " .. tGuildAndCircles.guild end
        for _, v in ipairs(tGuildAndCircles.circles) do
            for _, v2 in ipairs(tData.creator.circles) do
                if v == v2 then
                    if not hasCircle then 
                        strMatching = strMatching .. "\nCircles:"  
                        hasCircle = true
                    end
                    strMatching = strMatching .. "\n" .. v
                    break
                end
            end
        end
    end
    return strMatching
end

function GroupFinder:hasbit(x, p)
    return x % (p + p) >= p
end

function GroupFinder:notNil(strText, strReplace)
    return (strText == nil and strReplace or strText)
end