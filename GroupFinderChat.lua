local GroupFinder = Apollo.GetAddon("GroupFinder")

function GroupFinder:InitChat()
    Apollo.RegisterEventHandler("ChatMessage", "OnChatMessage", self)
end

function GroupFinder:IsLFGMessage(strMsg)
    return (
        (string.find(string.lower(strMsg), 'lfg') ~= nil)
        or(string.find(string.lower(strMsg), 'lfm') ~= nil)
        or(string.find(string.lower(strMsg), ' lf ') ~= nil)
        or(string.match(string.lower(strMsg), 'lf%dm') ~= nil)
        or(string.find(string.lower(strMsg), 'need heal') ~= nil)
        or(string.find(string.lower(strMsg), 'need tank') ~= nil)
        or(string.find(string.lower(strMsg), 'need dps') ~= nil)
        or(string.find(string.lower(strMsg), 'healer needed') ~= nil)
        or(string.find(string.lower(strMsg), 'tank needed') ~= nil)
        or(string.find(string.lower(strMsg), 'dps needed') ~= nil)
        or(string.find(string.lower(strMsg), 'looking for') ~= nil)
        or(string.find(string.lower(strMsg), 'anyone for') ~= nil)
        or(string.sub(string.lower(strMsg), 1, 3) == 'lf ')
    )
end

function GroupFinder:OnChatMessage(tChannel, tMessage)
    if tMessage.strSender ~= nil and tMessage.strSender ~= '' and tChannel:GetName() ~= 'Whisper' then

        local strMessage = tMessage.arMessageSegments[1].strText
        if self:IsLFGMessage(strMessage) then
            self:SetChatListing(tMessage.strSender, strMessage)
        end
    end
end

function GroupFinder:SetChatListing(strCreator, strGameNotes)
    local tCreator = { name = strCreator }
    local tListing = { listingType = self.ListingType.CHAT, notes = strGameNotes, time = os.time(), creator = tCreator }
    self:UpdateListing(tListing, true)
end