local GroupFinder = Apollo.GetAddon("GroupFinder")

local GameModeIcon = {
    ["Adventure"] = "IconSprites:Icon_Achievement_Achievement_Adventures",
    ["Arena"] = "IconSprites:Icon_Achievement_Achievement_PvP",
    ["Battleground"] = "IconSprites:Icon_Achievement_Achievement_PvP",
    ["Custom"] = "IconSprites:Icon_Achievement_Achievement_MetaAchievement",
    ["Dungeon"] = "IconSprites:Icon_Achievement_Achievement_Dungeon",
    ["Raid"] = "IconSprites:Icon_Achievement_Achievement_Raid",
    ["Shiphand"] = "IconSprites:Icon_Achievement_Achievement_Shiphand",
    ["Warplot"] = "IconSprites:Icon_Achievement_Achievement_PvP",
    ["Worldboss"] = "IconSprites:Icon_Achievement_Achievement_WorldEvent"
}

GroupFinder.tListingCounters = {}

function GroupFinder:SetupWindows()
    self.wndFindPremade = Apollo.LoadForm(self.xmlDoc, "FindPremade", nil, self)
    self.wndFindPremadeNavigation = Apollo.LoadForm(self.xmlDoc, "Navigation", self.wndFindPremade:FindChild("BGArt"), self)
    self.wndFindPremade:Show(false)

    self.wndCreatePremade = Apollo.LoadForm(self.xmlDoc, "CreatePremade", nil, self)
    self.wndCreatePremadeNavigation = Apollo.LoadForm(self.xmlDoc, "Navigation", self.wndCreatePremade:FindChild("BGArt"), self)
    self.wndCreatePremade:Show(false)

    self.wndOutdated = Apollo.LoadForm(self.xmlDoc, "Outdated", nil, self)
    self.wndOutdated:Show(false)

    self.wndStartPremadeButton = self.wndCreatePremade:FindChild("StartPremadeButton")
    self.wndStartPremadeButton:Enable(false)

    self.wndStopPremadeButton = self.wndCreatePremade:FindChild("StopPremadeButton")
    self.wndUpdatePremadeButton = self.wndCreatePremade:FindChild("UpdatePremadeButton")

    self.wndFindPremadeWhisperButton = self.wndFindPremade:FindChild("WhisperButton")
    self.wndFindPremadeWhisperButton:Enable(false)
    self.wndFindPremadeJoinButton = self.wndFindPremade:FindChild("JoinButton")
    self.wndFindPremadeJoinButton:Enable(false)
    self.wndFindPremadeRemoveButton = self.wndFindPremade:FindChild("RemoveButton")
    self.wndFindPremadeRemoveButton:Enable(false)

    self.wndListingsFrame = self.wndFindPremade:FindChild("ListingsFrame")
    self.wndLFMList = self.wndFindPremade:FindChild("LFMListings")
    self.wndLFGList = self.wndFindPremade:FindChild("LFGListings")
    self.wndChatList = self.wndFindPremade:FindChild("ChatListings")
    self.wndGameModeList = self.wndCreatePremade:FindChild("ModeList")
    self.wndInfoFlyOut = self.wndFindPremade:FindChild("InfoFlyOut")

    self.wndCurrentList = self.wndLFMList

    self.wndModeFlyOut = self.wndCreatePremade:FindChild("ModeFlyOut")
    self.wndModeFlyOutList = self.wndModeFlyOut:FindChild("ModeFlyOutList")
    self.wndModeFlyOutToggle = self.wndCreatePremade:FindChild("ModeFlyOutToggle")
    self.wndModeFlyOutToggle:AttachWindow(self.wndModeFlyOut)

    self.wndInformationFrame = self.wndCreatePremade:FindChild("InformationFrame")
    self.wndGameFrame = self.wndCreatePremade:FindChild("GameFrame")
    self.wndBlockModeFrame = self.wndGameFrame:FindChild("BlockModeFrame")
    self.wndBlockModeSort = self.wndGameFrame:FindChild("BlockModeSort")

    self.wndCreateLookingFlyOut = self.wndInformationFrame:FindChild("CreateLookingFlyOut")
    self.wndCreateLookingFlyOutList = self.wndCreateLookingFlyOut:FindChild("CreateLookingFlyOutList")
    self.wndCreateLookingFlyOutToggle = self.wndInformationFrame:FindChild("CreateLookingFlyOutToggle")
    self.wndCreateLookingFlyOutToggle:AttachWindow(self.wndCreateLookingFlyOut)

    self.wndFindLookingFlyOut = self.wndFindPremade:FindChild("FindLookingFlyOut")
    self.wndFindLookingFlyOutList = self.wndFindLookingFlyOut:FindChild("FindLookingFlyOutList")
    self.wndFindLookingFlyOutToggle = self.wndFindPremade:FindChild("FindLookingFlyOutToggle")
    self.wndFindLookingFlyOutToggle:AttachWindow(self.wndFindLookingFlyOut)

    self.wndGameNotes = self.wndInformationFrame:FindChild("Notes")
    self.wndGameNotes:SetMaxTextLength(250)
    self.wndCharactersLeft = self.wndInformationFrame:FindChild("CharactersLeft")
    self.wndTankRole = self.wndInformationFrame:FindChild("TankButton")
    self.wndHealerRole = self.wndInformationFrame:FindChild("HealerButton")
    self.wndDPSRole = self.wndInformationFrame:FindChild("DPSButton")
    self.wndVeteranMode = self.wndInformationFrame:FindChild("ToggleVeteranButton")
end


function GroupFinder:DrawGameModes()
    local wndList = self.wndGameModeList
    wndList:DeleteAll()

    local nIdx = 1
    local strTypeFilter = self.wndModeFlyOutToggle:FindChild("Label"):GetText()
    for _, v in ipairs(self.tGameModeList) do
        if (strTypeFilter == "Show All" and not v.hideInOverview) or v.gameType == strTypeFilter then
            wndList:AddRow("")
            wndList:SetCellLuaData(nIdx, 1, v)
            wndList:SetCellText(nIdx, 1, v.gameType)
            wndList:SetCellText(nIdx, 2, self:notNil(v.reqLevel, 0))
            wndList:SetCellText(nIdx, 3, v.name)
            wndList:SetCellText(nIdx, 4, self:notNil(v.maxPlayers, 0))
            wndList:SetCellText(nIdx, 5, string.format("%04d", self:notNil(v.reqLevel, 0)))
            wndList:SetCellText(nIdx, 6, string.format("%04d", self:notNil(v.maxPlayers, 0)))
            nIdx = nIdx + 1
        end
    end

    if strTypeFilter ~= "Show All" then
        wndList:SetSortColumn(2, true)
        self.wndBlockModeSort:Show(true)
    else
        wndList:SetSortColumn(1, true)
        self.wndBlockModeSort:Show(false)
    end

    self:SetCreatePremadeButtonState()
end


function GroupFinder:DrawModeFlyOut()
    self.wndModeFlyOutList:DestroyChildren()

    if not self.tGameTypeList then
        self.tGameTypeList = {}
        local tGameTypes = {}
        for _, v in ipairs(self.tGameModeList) do
            if tGameTypes[v.gameType] == nil then
                tGameTypes[v.gameType] = 1
                table.insert(self.tGameTypeList, v.gameType)
            end
        end

        table.sort(self.tGameTypeList)
        table.insert(self.tGameTypeList, 1, "Show All")
    end

    for _, v in ipairs(self.tGameTypeList) do
        local wnd = self:CreateButton("ModeButton", self.wndModeFlyOutList, v, "FilterGameMode", nil)
        if v == self.wndModeFlyOutToggle:FindChild("Label"):GetText() then
            wnd:FindChild("Button"):Enable(false)
        end
    end

    local tCurrentOffsets = { self.wndModeFlyOut:GetAnchorOffsets() }
    local tButtonPosition = { self.wndModeFlyOutToggle:GetAnchorOffsets() }
    self.wndModeFlyOut:SetAnchorOffsets(tCurrentOffsets[1], tCurrentOffsets[2], tCurrentOffsets[3], tButtonPosition[4] + (#self.tGameTypeList * 25) + 12)

    self.wndModeFlyOutList:ArrangeChildrenTiles()
end

function GroupFinder:DrawCreateLookingFlyOut()
    self.wndCreateLookingFlyOutList:DestroyChildren()

    local nBtnHeight
    for _, v in ipairs(self.ListingTypeData) do
        if v.showInCreateListing then
            local wnd = self:CreateButton("CreateLookingButton", self.wndCreateLookingFlyOutList, v.name, "ToggleCreateLooking", v)
            nBtnHeight = wnd:GetHeight()
        end
    end

    local tCurrentOffsets = { self.wndCreateLookingFlyOut:GetAnchorOffsets() }
    self.wndCreateLookingFlyOut:SetAnchorOffsets(tCurrentOffsets[1], tCurrentOffsets[2], tCurrentOffsets[3], ((#self.ListingTypeData - 1) * nBtnHeight) + 60)

    self.wndCreateLookingFlyOutList:ArrangeChildrenTiles()
end

function GroupFinder:DrawFindLookingFlyOut()
    self.wndFindLookingFlyOutList:DestroyChildren()

    local nBtnHeight
    for _, v in ipairs(self.ListingTypeData) do
        local wnd = self:CreateButton("FindLookingButton", self.wndFindLookingFlyOutList, v.name, "ToggleFindLooking", v)
        nBtnHeight = wnd:GetHeight()
    end

    local currentOffsets = { self.wndFindLookingFlyOut:GetAnchorOffsets() }
    self.wndFindLookingFlyOut:SetAnchorOffsets(currentOffsets[1], currentOffsets[2], currentOffsets[3], (#self.ListingTypeData * nBtnHeight) + 48)

    self.wndFindLookingFlyOutList:ArrangeChildrenTiles()
end

function GroupFinder:DrawPremadeList()

    local tPremadePreviousSorting = { column = self.wndCurrentList:GetSortColumn(), sortAscending = self.wndCurrentList:IsSortAscending() }

    self.wndCurrentList:DeleteAll()

    for _, v in ipairs(self.tPremadeList) do
        if v.listingType == self.nFindListingType then
            self:AddRowToPremadeList(v)
        end
    end

    if #self.tPremadeList > 0 then
        if not self.bIsRemoveListingTimerOn then
            self.bIsRemoveListingTimerOn = true
            Apollo.StartTimer("BroadcastRemoveTimeoutListings")
        end
    else
        if self.bIsRemoveListingTimerOn then
            self.bIsRemoveListingTimerOn = false
            Apollo.StopTimer("BroadcastRemoveTimeoutListings")
        end
    end

    --Set to previous sorting
    if tPremadePreviousSorting.column then
        self.wndCurrentList:SetSortColumn(tPremadePreviousSorting.column, tPremadePreviousSorting.sortAscending)
    end

    --Mark the previous row after an update
    local nIdx = 1
    if self.tPremadePreviousData then
        for _, v in ipairs(self.tPremadeList) do
            if v.listingType == self.nFindListingType then
                if self.tPremadePreviousData and v.creator.name == self.tPremadePreviousData.creator.name then
                    self.wndCurrentList:SetCurrentRow(nIdx)
                end
                nIdx = nIdx + 1
            end
        end
    end

    self:DrawPremadeCount()
end

function GroupFinder:DrawPremadeCount()
    local tListings = { 0, 0, 0 }
    if self.wndCurrentFrame then
  	    for _, v in ipairs(self.tPremadeList) do
    	    tListings[v.listingType] = tListings[v.listingType] and tListings[v.listingType] + 1 or 1
  	    end

  	    for i, v in ipairs(self.tListingCounters) do
    	    v[1]:SetText(tListings[i])
  	    end

        self.wndCurrentFrame:FindChild("Navigation"):FindChild("Counter"):SetText(#self.tPremadeList)
    end
    return tListings
end

function GroupFinder:SetLFMListGenerateTooltip(wndHandler, wndControl, eType, iRow, iColumn)
    local tData = wndHandler:GetCellData(iRow + 1, 1)
    local strTooltip
    if tData then
        if iColumn == 1 then
            strTooltip = tData.gameType
        elseif iColumn == 2 and tData.veteran == true then
            strTooltip = "Veteran"
        elseif iColumn == 4 then
            strTooltip = self:GetMatchingGuildAndCircles(tData)
        elseif iColumn == 6 and string.find(tData.notes, "\n") then
            strTooltip = tData.notes
        elseif iColumn == 7 then
            strTooltip = self:GetMembers(tData)
        elseif iColumn == 8 and self:hasbit(tData.roles, 1) then
            strTooltip = "Tank"
        elseif iColumn == 9 and self:hasbit(tData.roles, 2) then
            strTooltip = "Healer"
        elseif iColumn == 10 and self:hasbit(tData.roles, 4) then
            strTooltip = "DPS"
        end
    end
    wndHandler:SetTooltip(strTooltip and strTooltip or "")
end

function GroupFinder:SetLFGListGenerateTooltip(wndHandler, wndControl, eType, iRow, iColumn)
    local tData = wndHandler:GetCellData(iRow + 1, 1)
    local strTooltip
    if tData then
        if iColumn == 1 then
            strTooltip = tData.gameType
        elseif iColumn == 2 and tData.veteran == true then
            strTooltip = "Veteran"
        elseif iColumn == 4 then
            strTooltip = self:GetMatchingGuildAndCircles(tData)
        elseif iColumn == 6 and string.find(tData.notes, "\n") then
            strTooltip = tData.notes
        elseif iColumn == 7 and self:hasbit(tData.roles, 1) then
            strTooltip = "Tank"
        elseif iColumn == 8 and self:hasbit(tData.roles, 2) then
            strTooltip = "Healer"
        elseif iColumn == 9 and self:hasbit(tData.roles, 4) then
            strTooltip = "DPS"
        end
    end
    wndHandler:SetTooltip(strTooltip and strTooltip or "")
end

function GroupFinder:AddRowToPremadeList(tData)
    if self.nFindListingType == self.ListingType.LFM then
        return self:AddRowToLFMList(tData)
    elseif self.nFindListingType == self.ListingType.LFG then
        return self:AddRowToLFGList(tData)
    else
        return self:AddRowToChatList(tData)
    end
end

function GroupFinder:AddRowToLFMList(tData)
    local wndList = self.wndCurrentList
    local nIdx = wndList:AddRow("")

    wndList:SetCellLuaData(nIdx, 1, tData)
    wndList:SetCellImage(nIdx, 2, GameModeIcon[tData.gameType])
    if tData.veteran == true then
        wndList:SetCellImage(nIdx, 3, "IconSprites:Icon_Windows_UI_CRB_Coin_War")
    end
    wndList:SetCellText(nIdx, 4, " " .. tData.name)
    
    local tGuildAndCircles = self:GetGuildAndCircles()
    if tData.creator.guild == tGuildAndCircles.guild then
        wndList:SetCellImage(nIdx, 5, "IconSprites:Icon_Achievement_Achievement_SocialCircles")
    end

    wndList:SetCellText(nIdx, 6, " " .. tData.creator.name)
    wndList:SetCellText(nIdx, 7, " " .. string.sub(tData.notes, 1, string.find(tData.notes, "\n")))
    wndList:SetCellText(nIdx, 8, tData.players)

    if self:hasbit(tData.roles, 1) then
        wndList:SetCellImage(nIdx, 9, "IconSprites:Icon_Windows_UI_CRB_Attribute_Shield")
    end
    if self:hasbit(tData.roles, 2) then
        wndList:SetCellImage(nIdx, 10, "IconSprites:Icon_Windows_UI_CRB_Attribute_Health")
    end
    if self:hasbit(tData.roles, 4) then
        wndList:SetCellImage(nIdx, 11, "IconSprites:Icon_Windows_UI_CRB_Attribute_BruteForce")
    end

    return nIdx
end

function GroupFinder:AddRowToLFGList(tData)
    local wndList = self.wndCurrentList
    local nIdx = wndList:AddRow("")

    wndList:SetCellLuaData(nIdx, 1, tData)
    wndList:SetCellImage(nIdx, 2, GameModeIcon[tData.gameType])
    if tData.veteran == true then
        wndList:SetCellImage(nIdx, 3, "IconSprites:Icon_Windows_UI_CRB_Coin_War")
    end
    wndList:SetCellText(nIdx, 4, " " .. tData.name)

    local tGuildAndCircles = self:GetGuildAndCircles()
    if tData.creator.guild == tGuildAndCircles.guild then
        wndList:SetCellImage(nIdx, 5, "IconSprites:Icon_Achievement_Achievement_SocialCircles")
    end

    wndList:SetCellText(nIdx, 6, " " .. tData.creator.name)
    wndList:SetCellText(nIdx, 7, " " .. string.sub(tData.notes, 1, string.find(tData.notes, "\n")))

    if self:hasbit(tData.roles, 1) then
        wndList:SetCellImage(nIdx, 8, "IconSprites:Icon_Windows_UI_CRB_Attribute_Shield")
    end
    if self:hasbit(tData.roles, 2) then
        wndList:SetCellImage(nIdx, 9, "IconSprites:Icon_Windows_UI_CRB_Attribute_Health")
    end
    if self:hasbit(tData.roles, 4) then
        wndList:SetCellImage(nIdx, 10, "IconSprites:Icon_Windows_UI_CRB_Attribute_BruteForce")
    end

    return nIdx
end

function GroupFinder:AddRowToChatList(tData)
    if type(tData.creator) ~= "table" or type(tData.notes) ~= "string" or type(tData.time) ~= "number" then return false end

    local wndList = self.wndCurrentList
    local nIdx = wndList:AddRow("")
    wndList:SetCellLuaData(nIdx, 1, tData)
    wndList:SetCellText(nIdx, 1, "  " .. tData.creator.name)
    wndList:SetCellText(nIdx, 2, " " .. string.sub(tData.notes, 1, string.find(tData.notes, "\n")))
    wndList:SetCellText(nIdx, 3, " " .. os.date("%Y-%m-%d %H:%M", tData.time))

    return nIdx
end

function GroupFinder:SetChatListGenerateTooltip(wndHandler, wndControl, eType, iRow, iColumn)
    local tData = wndHandler:GetCellData(iRow + 1, 1)
    local strTooltip
    if tData then
        if iColumn == 2 and string.find(tData.notes, "\n") then
            strTooltip = tData.notes
        end
    end
    wndHandler:SetTooltip(strTooltip and strTooltip or "")
end
