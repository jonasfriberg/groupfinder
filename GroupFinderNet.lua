local GroupFinder = Apollo.GetAddon("GroupFinder")

local LibJSON = Apollo.GetPackage("Lib:dkJSON-2.5").tPackage

local MESSAGE_VERSION = 0.02

local MessageType = {
    REQUEST_LISTING = 1,
    UPDATE_LISTING = 2,
    REMOVE_LISTING = 3
}

local ICCommMessageResult = {
  [ICCommLib.CodeEnumICCommMessageResult.InvalidText] = "Invalid Text", 
  [ICCommLib.CodeEnumICCommMessageResult.MissingEntitlement] = "Missing Entitlement", 
  [ICCommLib.CodeEnumICCommMessageResult.NotInChannel] = "Not In Channel", 
  [ICCommLib.CodeEnumICCommMessageResult.Sent] = "Sent", 
  [ICCommLib.CodeEnumICCommMessageResult.Throttled] = "Throttled"
}

local ICCommJoinResult = {
  [ICCommLib.CodeEnumICCommJoinResult.BadName] = "Bad Name", 
  [ICCommLib.CodeEnumICCommJoinResult.Join] = "Join", 
  [ICCommLib.CodeEnumICCommJoinResult.Left] = "Left", 
  [ICCommLib.CodeEnumICCommJoinResult.MissingEntitlement] = "Missing Entitlement", 
  [ICCommLib.CodeEnumICCommJoinResult.NoGroup] = "No Group",
  [ICCommLib.CodeEnumICCommJoinResult.NoGuild] = "No Guild",
  [ICCommLib.CodeEnumICCommJoinResult.TooManyChannels] = "Too Many Channels"
}

function GroupFinder:InitNetwork()

    Apollo.RegisterTimerHandler("UpdateCommChannel", "UpdateCommChannel", self)
    Apollo.CreateTimer("UpdateCommChannel", 1, false)
    Apollo.StartTimer("UpdateCommChannel")

    Apollo.RegisterTimerHandler("BroadcastInitialRequest", "BroadcastRequestListings", self)
    Apollo.CreateTimer("BroadcastInitialRequest", 5, false)
    Apollo.StartTimer("BroadcastInitialRequest")

    Apollo.RegisterTimerHandler("BroadcastPeriodicListingUpdate", "PeriodicListingUpdate", self)
    Apollo.CreateTimer("BroadcastPeriodicListingUpdate", self.Timers["Periodic_Update"], true)
    Apollo.StopTimer("BroadcastPeriodicListingUpdate")

    Apollo.RegisterTimerHandler("BroadcastRemoveTimeoutListings", "RemoveTimeoutListings", self)
    Apollo.CreateTimer("BroadcastRemoveTimeoutListings", self.Timers["Periodic_Remove"], true)
    Apollo.StopTimer("BroadcastRemoveTimeoutListings")
  
end

function GroupFinder:OnSendMessageResultEvent(iccomm, eResult, idMessage)
    --SendVarToRover("OnSendMessageResultEvent", { eResult = eResult, message = ICCommMessageResult[eResult], idMessage = idMessage }, 0)
end

function GroupFinder:OnJoinResultEvent(iccomm, eResult)
    --SendVarToRover("OnJoinResultEvent", { eResult = eResult, message = ICCommJoinResult[eResult] }, 0)
end

function GroupFinder:OnThrottledEvent(iccomm, strSender, idMessage)
    --SendVarToRover("OnThrottledEvent", { strSender = strSender, idMessage = idMessage }, 0)
end

function GroupFinder:UpdateCommChannel()
    if not self.tCommChannel then
        self.tCommChannel = ICCommLib.JoinChannel("GroupFinder", ICCommLib.CodeEnumICCommChannelType.Global)
    end

    if self.tCommChannel:IsReady() then
        self.tCommChannel:SetSendMessageResultFunction("OnSendMessageResultEvent", self)
        self.tCommChannel:SetJoinResultFunction("OnJoinResultEvent", self)
        self.tCommChannel:SetThrottledFunction("OnThrottledEvent", self)

        self.tCommChannel:SetReceivedMessageFunction("OnBroadcastReceived", self)
    else
		Apollo.StartTimer("UpdateCommChannel")
 	end
end

function GroupFinder:BroadcastMessage(nMsgType, tData)
    if not self.tCommChannel then
        self:UpdateCommChannel()
    else
        local tMsg = { msgType = nMsgType, data = tData, version = MESSAGE_VERSION, addonVersion = self.ADDON_VERSION }
--        SendVarToRover("BroadcastMessage", tMsg, 0)
--        SendVarToRover("BroadcastMessage-encoded", LibJSON.encode(tMsg), 0)
--        SendVarToRover("Length", string.len(LibJSON.encode(tMsg)), 0)
        self.tCommChannel:SendMessage(LibJSON.encode(tMsg))
    end
end

function GroupFinder:BroadcastRequestListings()
    self:BroadcastMessage(MessageType.REQUEST_LISTING, nil)
end

function GroupFinder:BroadcastUpdateListing(tListing)
    --Print("Updating Listing")
    self:BroadcastMessage(MessageType.UPDATE_LISTING, tListing)
end

function GroupFinder:BroadcastRemoveListing(tListing)
    self:BroadcastMessage(MessageType.REMOVE_LISTING, tListing)
end

function GroupFinder:OnBroadcastReceived(tChannel, strMsg)
    if string.sub(strMsg, 1, 1) ~= "{" then 
        --SendVarToRover("Not a JSON", strMsg, 0)
        return 
    end
    
	local tMsg = LibJSON.decode(strMsg)
    --SendVarToRover("OnBroadcastReceived", tMsg, 0)
    if tMsg ~= nil and self:isVersionUpToDate(tMsg) then
        if tMsg.msgType == MessageType.REQUEST_LISTING and self.tOwnListing ~= nil then
            self:UpdateListing(self.tOwnListing, false)
        elseif tMsg.msgType == MessageType.UPDATE_LISTING then
            self:UpdateListing(tMsg.data, false)
        elseif tMsg.msgType == MessageType.REMOVE_LISTING then
            self:RemoveListing(tMsg.data)
        end
    end
end

function GroupFinder:UpdateListing(tData, bBroadcast)

    if self:ValidateBroadcast(tData) then

        local nIdx = 0
        for i, v in ipairs(self.tPremadeList) do
            if v.creator.name == tData.creator.name and v.listingType == tData.listingType then nIdx = i break end
        end

        if nIdx > 0 then
            self.tPremadeList[nIdx] = tData
        else
            table.insert(self.tPremadeList, tData)
        end

        if tData.creator.name == self:GetPlayerName() then
            self.tOwnListing = tData
        end

        if bBroadcast then
            self:BroadcastUpdateListing(tData)
        end

        self:DrawPremadeList()
    end
end

function GroupFinder:RemoveListing(tData)
    for i, v in ipairs(self.tPremadeList) do
        if v.listingType == tData.listingType and v.creator.name == tData.creator.name then
            table.remove(self.tPremadeList, i)
        end
    end

    if tData.listingType ~= self.ListingType.CHAT and tData.creator.name == self:GetPlayerName() then
        self:BroadcastRemoveListing({ listingType = tData.listingType, creator = { name = tData.creator.name } })
    end

    self:DrawPremadeList()
end

function GroupFinder:ValidateBroadcast(tData)

    if tData.listingType == self.ListingType.CHAT then
        
        if tData.creator and tData.notes and tData.time then return true else return false end

    else
        if tData.listingType and tData.gameType and tData.name and tData.players and tData.time and tData.creator then

            if tData.listingType ~= self.ListingType.LFM and tData.listingType ~= self.ListingType.LFG then return false end

            local bExists = false
            for _, v in ipairs(self.tGameModeList) do
                if v.gameType == tData.gameType and v.name == tData.name then bExists = true end
            end
            if not bExists then return false end

            local nPlayerMatch = string.find(tData.players, "%d+%s/%s%d+")
            if not nPlayerMatch or nPlayerMatch ~= 1 or string.len(nPlayerMatch) > 7 then return false end

            -- timezones?
            --if os.difftime(os.time(), tData.time) > 30 then return false end

            -- if GameLib.GetPlayerUnitByName(data.creator) == nil then return false end
        else
            return false
        end
    end

    return true
end

function GroupFinder:isVersionUpToDate(tMsg)
    if tMsg.version and tMsg.version > MESSAGE_VERSION then
        self:ShowUpdateAddonInfo()
        return false
    end
    if tMsg.addonVersion and tonumber(string.sub(tMsg.addonVersion, 1, 3)) > tonumber(self.ADDON_VERSION) then
        self:ShowUpdateAddonInfo()
    end
    return true
end